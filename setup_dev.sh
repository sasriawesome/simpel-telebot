#!/bin/sh

rm -rf ./staticfiles || true
rm -rf ./mediafiles || true
mkdir ./staticfiles || true
mkdir ./mediafiles || true
chmod 777 ./staticfiles || true
chmod 777 ./mediafiles || true
docker-compose -f docker-compose.dev.yml down
docker-compose -f docker-compose.dev.yml up -d
