import json

from django.contrib.auth.decorators import login_required
from django.http import JsonResponse
from django.utils.decorators import method_decorator
from django.views.generic import FormView, View

from simpel_telebot.forms import BroadcastForm

from .settings import telebot_settings
from .tasks import process_telegram_event
from .utils import get_logger

logger = get_logger()


@method_decorator([login_required], name="dispatch")
class BroadcastView(FormView):
    form_class = BroadcastForm
    template_name = "telebot/broadcast_form.html"

    def form_valid(self, form):
        form.send(self.request)
        return super().form_valid(form)


class TelebotWebhookView(View):
    # WARNING: if fail - Telegram webhook will be delivered again.
    # Can be fixed with async celery task execution
    def post(self, request, *args, **kwargs):
        data = json.loads(request.body)
        if telebot_settings.DEBUG:
            process_telegram_event(data)
        else:
            # Process Telegram event in worker (async)
            process_telegram_event.delay(data)

        # There is a great trick to send action in webhook response
        # e.g. remove buttons, typing event
        return JsonResponse({"ok": "POST request processed"})

    def get(self, request, *args, **kwargs):  # for debug
        return JsonResponse({"ok": "Get request received! But nothing done"})
