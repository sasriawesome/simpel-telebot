import datetime

from django.utils import timezone

from telegram import ParseMode, Update
from telegram.ext import CallbackContext

from ...models import TelegramUser
from ..utils.info import extract_user_data_from_update
from . import static_text
from .keyboards import make_keyboard_for_start_command


def command_start(update: Update, context: CallbackContext):

    u, created = TelegramUser.get_user_and_created(update, context)

    if created:
        text = static_text.start_created.format(first_name=u.first_name)
    else:
        text = static_text.start_not_created.format(first_name=u.first_name)

    update.message.reply_text(text=text, reply_markup=make_keyboard_for_start_command())


def secret_level(update: Update, context: CallbackContext):
    # callback_data: SECRET_LEVEL_BUTTON variable from manage_data.py
    """Pressed 'secret_level_button_text' after /start command"""

    user_id = extract_user_data_from_update(update)["user_id"]
    text = static_text.unlock_secret_room.format(
        user_count=TelegramUser.objects.count(),
        active_24=TelegramUser.objects.filter(updated_at__gte=timezone.now() - datetime.timedelta(hours=24)).count(),
    )

    context.bot.edit_message_text(
        text=text, chat_id=user_id, message_id=update.callback_query.message.message_id, parse_mode=ParseMode.HTML
    )
