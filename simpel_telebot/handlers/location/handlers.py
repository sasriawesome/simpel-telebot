import telegram
from telegram import Update
from telegram.ext import CallbackContext

from .keyboards import send_location_keyboard
from .static_text import share_location, thanks_for_location


def ask_for_location(update: Update, context: CallbackContext):
    """Entered /ask_location command"""
    from ...models import TelegramUser
    u = TelegramUser.get_user(update, context)

    context.bot.send_message(chat_id=u.user_id, text=share_location, reply_markup=send_location_keyboard())


def location_handler(update: Update, context: CallbackContext):
    # receiving user's location
    from ...models import Location, TelegramUser
    u = TelegramUser.get_user(update, context)
    lat, lon = update.message.location.latitude, update.message.location.longitude
    Location.objects.create(user=u, latitude=lat, longitude=lon)

    update.message.reply_text(
        thanks_for_location,
        reply_markup=telegram.ReplyKeyboardRemove(),
    )
