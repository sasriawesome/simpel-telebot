import uuid


def get_logger():
    class Logger(object):
        def info(self, text):
            print(str(text))

        def error(self, text):
            print(str(text))

    return Logger()


def generate_uid(model, field_name="uid"):
    """
    Generate a new code

    :model: Model
    :uid_fields: Character set to choose from
    :max_len: Max length default 8
    """
    code = uuid.uuid4()
    # Ensure code does not aleady exist
    try:
        kwargs = {field_name: code}
        model.objects.get(**kwargs)
    except model.DoesNotExist:
        return code
    return generate_uid(model, field_name)
