"""
    Telegram event handlers
"""

# from dtb.celery import app  # event processing in async mode
from telegram import Bot, BotCommand
from telegram.ext import CallbackQueryHandler, CommandHandler, Filters, MessageHandler

from .handlers.admin import handlers as admin_handlers
from .handlers.broadcast import handlers as broadcast_handlers
from .handlers.broadcast.manage_data import CONFIRM_DECLINE_BROADCAST
from .handlers.broadcast.static_text import broadcast_command
from .handlers.location import handlers as location_handlers
from .handlers.onboarding import handlers as onboarding_handlers
from .handlers.onboarding.manage_data import SECRET_LEVEL_BUTTON
from .handlers.utils import error, files


def setup_commands(bot_instance: Bot):
    langs_with_commands = {
        "en": {
            "start": "Start django bot 🚀",
            "stats": "Statistics of bot 📊",
            "admin": "Show admin info ℹ️",
            "ask_location": "Send location 📍",
            "broadcast": "Broadcast message 📨",
            "export_users": "Export users.csv 👥",
        },
        "es": {
            "start": "Iniciar el bot de django 🚀",
            "stats": "Estadísticas de bot 📊",
            "admin": "Mostrar información de administrador ℹ️",
            "ask_location": "Enviar ubicación 📍",
            "broadcast": "Mensaje de difusión 📨",
            "export_users": "Exportar users.csv 👥",
        },
        "fr": {
            "start": "Démarrer le bot Django 🚀",
            "stats": "Statistiques du bot 📊",
            "admin": "Afficher les informations d'administrateur ℹ️",
            "ask_location": "Envoyer emplacement 📍",
            "broadcast": "Message de diffusion 📨",
            "export_users": "Exporter users.csv 👥",
        },
        "ru": {
            "start": "Запустить django бота 🚀",
            "stats": "Статистика бота 📊",
            "admin": "Показать информацию для админов ℹ️",
            "broadcast": "Отправить сообщение 📨",
            "ask_location": "Отправить локацию 📍",
            "export_users": "Экспорт users.csv 👥",
        },
    }

    bot_instance.delete_my_commands()
    for language_code in langs_with_commands:
        bot_instance.set_my_commands(
            language_code=language_code,
            commands=[
                BotCommand(command, description) for command, description in langs_with_commands[language_code].items()
            ],
        )


def setup_dispatcher(dp):
    """
    Adding handlers for events from Telegram
    """
    # onboarding
    dp.add_handler(CommandHandler("start", onboarding_handlers.command_start))

    # admin commands
    dp.add_handler(CommandHandler("admin", admin_handlers.admin))
    dp.add_handler(CommandHandler("stats", admin_handlers.stats))
    dp.add_handler(CommandHandler("export_users", admin_handlers.export_users))

    # location
    dp.add_handler(CommandHandler("ask_location", location_handlers.ask_for_location))
    dp.add_handler(MessageHandler(Filters.location, location_handlers.location_handler))

    # secret level
    dp.add_handler(CallbackQueryHandler(onboarding_handlers.secret_level, pattern=f"^{SECRET_LEVEL_BUTTON}"))

    # broadcast message
    dp.add_handler(
        MessageHandler(
            Filters.regex(rf"^{broadcast_command}(/s)?.*"), broadcast_handlers.broadcast_command_with_message
        )
    )
    dp.add_handler(
        CallbackQueryHandler(broadcast_handlers.broadcast_decision_handler, pattern=f"^{CONFIRM_DECLINE_BROADCAST}")
    )

    # files
    dp.add_handler(
        MessageHandler(
            Filters.animation,
            files.show_file_id,
        )
    )

    # handling errors
    dp.add_error_handler(error.send_stacktrace_to_tg_chat)

    # EXAMPLES FOR HANDLERS
    # dp.add_handler(MessageHandler(Filters.text, <function_handler>))
    # dp.add_handler(MessageHandler(
    #     Filters.document, <function_handler>,
    # ))
    # dp.add_handler(CallbackQueryHandler(<function_handler>, pattern="^r\d+_\d+"))
    # dp.add_handler(MessageHandler(
    #     Filters.chat(chat_id=int(TELEGRAM_FILESTORAGE_ID)),
    #     # & Filters.forwarded & (Filters.photo | Filters.video | Filters.animation),
    #     <function_handler>,
    # ))

    return dp
