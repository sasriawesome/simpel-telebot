from django.apps import AppConfig


class SimpelTelebotConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'simpel_telebot'
    labe = 'simpel_telebot'
    icon = 'robot-love-outline'
    verbose_name = "Simpel Telebot"
