from django.utils import timezone

from django_rq import job

from simpel_telebot.utils import get_logger

logger = get_logger()


@job
def process_telegram_event_for_bot(bot_uid, update_json):
    from .models import TelegramBot

    try:
        # get bot from database and instantiate
        bot = TelegramBot.objects.get(uid=bot_uid)
        logger.info("%s This is %s update event" % (timezone.now(), bot.instance.first_name))
    except TelegramBot.DoesNotExist:
        logger.info("Bot not found")
    except Exception as err:
        logger.error("There is an error here!! : %s" % err)
