from django import forms
from django.contrib.admin.widgets import FilteredSelectMultiple

from simpel_telebot.models import TelegramUser

from .models import Campaign


class AdminCampaignForm(forms.ModelForm):

    recipients = forms.ModelMultipleChoiceField(
        queryset=TelegramUser.objects.all(),
        widget=FilteredSelectMultiple("verbose name", is_stacked=False),
    )

    class Meta:
        model = Campaign
        fields = "__all__"

    def save(self, commit=True):
        instance = super().save(commit)
        return instance
