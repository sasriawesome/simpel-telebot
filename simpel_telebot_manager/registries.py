from django.utils.functional import cached_property

from telegram import Bot
from telegram.error import Unauthorized
from telegram.ext import Dispatcher

from simpel_telebot.helpers import setup_dispatcher
from simpel_telebot.utils import get_logger

logger = get_logger()


class HandlerRegisty(object):
    def register(self):
        return


class BotRegistry(object):
    @cached_property
    def instance(self):
        return self.get_bot()

    @cached_property
    def dispatcher(self):
        return self.get_dispatcher()

    def get_bot(self):
        bot = Bot(self.bot_token)
        try:
            TELEGRAM_BOT_USERNAME = bot.get_me()["username"]
            logger.info("%s wakeup!" % TELEGRAM_BOT_USERNAME)
        except Unauthorized as err:
            logger.error("Invalid TELEGRAM_TOKEN: %s" % err)
        return bot

    def setup_dispatcher(self, instance, dispatcher):
        handlers = instance.handlers.all()
        for handler in handlers:
            print("registering handlers")
        dispatcher.add_handler(instance.handler)
        return setup_dispatcher()

    def register(self, instance, **kwargs):
        """Register a bot"""
        # Don't bother registering this if it is already registered
        bot = Bot(instance.bot_token)
        try:
            TELEGRAM_BOT_USERNAME = bot.get_me()["username"]
            logger.info("%s wakeup!" % TELEGRAM_BOT_USERNAME)
        except Unauthorized as err:
            logger.error("Invalid TELEGRAM_TOKEN: %s" % err)
            return

        if instance.id in self:
            return instance

        dispatcher = Dispatcher(bot, update_queue=None, use_context=True)
        self.setup_dispatcher(dispatcher)

        self[instance.id] = {
            "instance": instance,
            "dispatcher": self.setup_dispatcher(instance, bot),
        }


bot_registry = BotRegistry()
