from django.apps import AppConfig


class SimpelTelebotManagerConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'simpel_telebot_manager'
